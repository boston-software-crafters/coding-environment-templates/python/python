# python

python starter project

# To Install Requirements

pip install -r requirements.txt

# To Run the Tests

pytest

# To Run the Tests with Coverage

pytest --cov=kata .

# To Run in Watch Mode with Coverage

ptw --runner "pytest --cov=kata"
